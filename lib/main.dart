import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

import 'package:watch_in_client/view/pages/home.dart';
import 'package:watch_in_client/view/themes.dart';


void main() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    print('${record.level.name}: ${record.time}: ${record.message}');
  });

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WatchIn Client',
      themeMode: ThemeMode.system,
      theme: Themes.lightTheme,
      darkTheme: Themes.darkTheme,
      home: const HomePage(title: 'WatchIn Client Home Page'),
    );
  }
}
