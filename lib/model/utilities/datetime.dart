const Map<int, String> months = {
  1: "January",
  2: "February",
  3: "March",
  4: "April",
  5: "May",
  6: "June",
  7: "July",
  8: "August",
  9: "September",
  10: "October",
  11: "November",
  12: "December",
};

String formDate(DateTime instant) {
  return "${instant.day} ${months[instant.month]} ${instant.year}";
}

String formTime(double time) {
  if (time > 3600) {
    return "${(time / 3600).floor()}:${(time / 60).floor()}:${(time % 60).floor()}";
  } else {
    return "${(time / 60).floor()}:${(time % 60).floor()}";
  }
}
