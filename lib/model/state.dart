import 'dart:io';

import 'package:watch_in_client/model/data/user.dart';


class State {
  static final State _singleton = State._internal();

  User? user;
  Cookie? cookie;

  factory State() {
    return _singleton;
  }

  State._internal();

  bool isAuthor(String user) {
    return this.user != null && this.user!.name == user;
  }

  bool isNotAuthor(String user) {
    return this.user != null && this.user!.name != user;
  }
}