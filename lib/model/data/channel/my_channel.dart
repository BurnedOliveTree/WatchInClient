import 'package:watch_in_client/model/data/resources.dart';

class MyChannel {
  String? description;
  Resource avatar;
  Resource? background;

  MyChannel({required this.description, required this.avatar, required this.background});

  factory MyChannel.fromJson(Map<String, dynamic> json) => MyChannel(
    description: json['description'] == null ? null : json['description'] as String,
    avatar: Resource.fromJson(json['avatar'] as Map<String, dynamic>),
    background: json['background'] == null ? null
        : Resource.fromJson(json['background'] as Map<String, dynamic>),
  );
}
