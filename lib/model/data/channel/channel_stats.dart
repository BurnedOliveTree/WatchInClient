class ChannelStatistics {
  Iterable<ChannelStatistic> stats;

  ChannelStatistics({required this.stats});

  factory ChannelStatistics.fromJson(List<dynamic> json) => ChannelStatistics(
    stats: json.map((stat) => ChannelStatistic.fromJson(stat)),
  );
}

class ChannelStatistic {
  DateTime date;
  int subscribers;
  int videos;
  int views;
  int comments;

  ChannelStatistic({required this.date, required this.subscribers,
    required this.videos, required this.views, required this.comments});

  factory ChannelStatistic.fromJson(Map<String, dynamic> json) => ChannelStatistic(
    date: DateTime.parse(json['date'] as String),
    subscribers: json['subscribers'] as int,
    videos: json['videos'] as int,
    views: json['views'] as int,
    comments: json['comments'] as int,
  );

  @override
  String toString() {
    return "Date: $date, subscribers: $subscribers, videos: $videos, views: $views, comments: $comments";
  }
}
