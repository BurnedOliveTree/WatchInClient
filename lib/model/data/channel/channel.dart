import 'package:watch_in_client/model/data/resources.dart';

class Channel {
  String name;
  Resource avatar;
  int subscriptionsCount;

  Channel({required this.name, required this.avatar, required this.subscriptionsCount});

  factory Channel.fromJson(Map<String, dynamic> json) => Channel(
    name: json['name'] as String,
    avatar: Resource.fromJson(json['avatar'] as Map<String, dynamic>),
    subscriptionsCount: json['subscriptionsCount'] as int,
  );
}
