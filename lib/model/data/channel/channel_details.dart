import 'package:watch_in_client/model/data/channel/channel.dart';
import 'package:watch_in_client/model/data/resources.dart';


class ChannelDetails extends Channel {
  DateTime creationDate;
  String? description;
  Resource? background;
  int videosCount;
  int viewsCount;

  ChannelDetails({
    required name,
    required this.creationDate,
    this.description,
    required avatar,
    this.background,
    required this.videosCount,
    required this.viewsCount,
    required subscriptionsCount,
  }) : super(
    name: name,
    avatar: avatar,
    subscriptionsCount: subscriptionsCount
  );

  factory ChannelDetails.fromJson(Map<String, dynamic> json) => ChannelDetails(
    name: json['name'] as String,
    creationDate: DateTime.parse(json['creationDate'] as String),
    description: json['description'] == null ? null : json['description'] as String,
    avatar: Resource.fromJson(json['avatar'] as Map<String, dynamic>),
    background: json['background'] == null ? null
        : Resource.fromJson(json['background'] as Map<String, dynamic>),
    videosCount: json['videosCount'] as int,
    viewsCount: json['viewsCount'] as int,
    subscriptionsCount: json['subscriptionsCount'] as int,
  );
}
