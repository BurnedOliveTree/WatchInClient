import 'package:watch_in_client/model/data/resources.dart';

class User {
  String mail;
  String name;
  Resource? avatar;

  User({required this.mail, required this.name, required this.avatar});

  factory User.fromJson(Map<String, dynamic> json) => User(
    mail: json['email'] as String,
    name: json['username'] as String,
    avatar: json['avatar'] == null
        ? null
        : Resource.fromJson(json['avatar'] as Map<String, dynamic>),
  );
}
