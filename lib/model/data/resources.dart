enum ResourceType {
  avatar,
  background,
  thumbnail,
  video
}

class Resource {
  String url;
  ResourceType type;

  Resource({required this.url, required this.type});

  factory Resource.fromJson(Map<String, dynamic> json) => Resource(
    url: json['location'] as String,
    type: ResourceType.values.byName((json['type'] as String).toLowerCase()),
  );
}

class VideoResource extends Resource {
  String quality;

  VideoResource({required this.quality, required url, required type, }) : super(url: url, type: type);

  factory VideoResource.fromJson(Map<String, dynamic> json) => VideoResource(
    url: json['resource']['location'] as String,
    type: ResourceType.values.byName((json['resource']['type'] as String).toLowerCase()),
    quality: json['quality']['friendlyName'] as String
  );
}
