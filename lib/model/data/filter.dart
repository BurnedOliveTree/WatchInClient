enum SortOrder {
  date, relevance, views;

  @override
  String toString() => name.toUpperCase();
}

enum SortDirection {
  asc, desc;

  @override
  String toString() => name.toUpperCase();
}

enum DateFilter {
  hour, day, week, month, year;

  @override
  String toString() => name.toUpperCase();
}

enum DurationFilter {
  short, medium, long;

  @override
  String toString() => name.toUpperCase();
}

class ManageableFilter {
  String? phrase;
  SortDirection? sortDirection;
  String? sortField;

  ManageableFilter({this.phrase, this.sortDirection, this.sortField});
}

class SearchFilter {
  String? phrase;
  String? channel;
  SortOrder? sort;
  DateFilter? date;
  DurationFilter? duration;

  SearchFilter({this.phrase, this.channel, this.sort, this.date, this.duration});
}
