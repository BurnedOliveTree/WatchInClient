import 'package:watch_in_client/model/data/channel/channel.dart';


class Comment {
  int id;
  String content;
  DateTime creationDate;
  Channel channel;

  Comment({required this.id, required this.content, required this.creationDate, required this.channel});

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
    id: json['id'] as int,
    content: json['content'] as String,
    creationDate: DateTime.parse(json['creationDate'] as String),
    channel: Channel.fromJson(json['channel'] as Map<String, dynamic>),
  );
}