import 'package:watch_in_client/model/api/media/video/video.dart';
import 'package:watch_in_client/model/data/channel/channel.dart';
import 'package:watch_in_client/model/data/resources.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/data/media/video/editable.dart';

class ManageableVideo extends EditableVideo implements ListableAndEditable {
  @override double length;
  @override DateTime uploaded;
  @override int views;
  @override Channel channel;

  ManageableVideo({
    required id,
    required title,
    description,
    required this.length,
    required this.uploaded,
    required this.views,
    required thumbnail,
    required visibility,
    required status,
    required this.channel,
  }) : super(
    id: id,
    title: title,
    description: description,
    thumbnail: thumbnail,
    visibility: visibility,
    status: status,
  );

  factory ManageableVideo.fromJson(Map<String, dynamic> json) => ManageableVideo(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] == null ? null : json['description'] as String,
    length: json['length'] as double,
    uploaded: DateTime.parse(json['uploaded'] as String),
    views: json['views'] as int,
    thumbnail: Resource.fromJson(json['thumbnail'] as Map<String, dynamic>),
    visibility: Visibility.fromString(json['visibility'] as String),
    status: Status.values.byName((json['status'] as String).toLowerCase()),
    channel: Channel.fromJson(json['author']),
  );

  @override
  Future<Watchable> toWatchable() async {
    return await VideoService.details(this);
  }
}