import 'package:watch_in_client/model/api/media/video/video_management.dart';
import 'package:watch_in_client/model/data/resources.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';

class EditableVideo implements Editable {
  @override String id;
  @override String title;
  @override String? description;
  Resource thumbnail;
  @override Visibility visibility;
  @override Status status;

  EditableVideo({
    required this.id,
    required this.title,
    this.description,
    required this.thumbnail,
    required this.visibility,
    required this.status,
  });

  factory EditableVideo.fromJson(Map<String, dynamic> json) => EditableVideo(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] == null ? null : json['description'] as String,
    thumbnail: Resource.fromJson(json['thumbnail'] as Map<String, dynamic>),
    visibility: Visibility.fromString(json['visibility'] as String),
    status: Status.values.byName((json['status'] as String).toLowerCase()),
  );

  @override
  Future<void> setTitle(String title) async {
    return await VideoManagementService.setTitle(this, title);
  }

  @override
  Future<void> setDescription(String? description) async {
    return description == null || description.isEmpty
      ? VideoManagementService.deleteDescription(this)
      : VideoManagementService.setDescription(this, description);
  }

  @override
  Future<void> setVisibility(Visibility visibility) async {
    return await VideoManagementService.setVisibility(this, visibility);
  }

  @override
  Future<void> setThumbnail(String? path) async {
    return path == null || path.isEmpty
        ? VideoManagementService.deleteThumbnail(this)
        : VideoManagementService.setThumbnail(this, path);
  }

  @override
  Future<void> delete() async {
    return await VideoManagementService.delete(this);
  }
}