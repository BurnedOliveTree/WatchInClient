import 'package:watch_in_client/model/api/media/video/video.dart';
import 'package:watch_in_client/model/data/channel/channel.dart';
import 'package:watch_in_client/model/data/resources.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';

class WatchableVideo implements Watchable {
  @override String id;
  @override String title;
  @override String? description;
  @override DateTime uploaded;
  @override double length;
  @override int views;
  @override Channel channel;
  @override List<VideoResource>? resources;
  @override Likes likes;
  @override bool favorite;
  @override bool watchLater;

  WatchableVideo({
    required this.id,
    required this.title,
    this.description,
    required this.uploaded,
    required this.length,
    required this.views,
    required this.channel,
    required this.resources,
    required this.likes,
    required this.favorite,
    required this.watchLater,
  });

  factory WatchableVideo.fromJson(Map<String, dynamic> json) => WatchableVideo(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] == null ? null : json['description'] as String,
    uploaded: DateTime.parse(json['uploaded'] as String),
    length: json['length'] as double,
    views: json['views'] as int,
    channel: Channel.fromJson(json['channel']),
    resources: (json['resources'] as List<dynamic>)
      .map((resource) => VideoResource.fromJson(resource))
      .toList(),
    likes: Likes.fromJson(json['likes']),
    favorite: json['favorite']['status'] as bool,
    watchLater: json['watchLater']['status'] as bool,
  );

  @override
  Future<void> view() async {
    return await VideoService.view(this);
  }
}
