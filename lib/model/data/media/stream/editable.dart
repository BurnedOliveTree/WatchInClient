import 'package:watch_in_client/model/api/media/stream/stream_management.dart';
import 'package:watch_in_client/model/data/channel/channel.dart';
import 'package:watch_in_client/model/data/resources.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';

class EditableStream implements Editable {
  @override String id;
  @override String title;
  @override String? description;
  @override Visibility visibility;
  @override Status status;
  String uploadUrl;
  String watchUrl;
  Channel channel;
  Resource thumbnail;

  EditableStream({
    required this.id,
    required this.title,
    this.description,
    required this.visibility,
    required this.status,
    required this.uploadUrl,
    required this.watchUrl,
    required this.channel,
    required this.thumbnail
  });

  factory EditableStream.fromJson(Map<String, dynamic> json) => EditableStream(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] == null ? null : json['description'] as String,
    visibility: Visibility.fromString(json['visibility'] as String),
    status: Status.streaming,
    uploadUrl: json['uploadUrl'] as String,
    watchUrl: json['watchUrl'] as String,
    channel: Channel.fromJson(json['author']),
    thumbnail: Resource.fromJson(json['thumbnail'] as Map<String, dynamic>),
  );

  @override
  Future<void> setTitle(String title) async {
    return await StreamManagementService.setTitle(this, title);
  }

  @override
  Future<void> setDescription(String? description) async {
    return description == null || description.isEmpty
        ? StreamManagementService.deleteDescription(this)
        : StreamManagementService.setDescription(this, description);
  }

  @override
  Future<void> setVisibility(Visibility visibility) async {
    return await StreamManagementService.setVisibility(this, visibility);
  }

  @override
  Future<void> setThumbnail(String? path) async {
    return path == null || path.isEmpty
        ? StreamManagementService.deleteThumbnail(this)
        : StreamManagementService.setThumbnail(this, path);
  }

  @override
  Future<void> delete() async {
    return await StreamManagementService.delete(this);
  }
}