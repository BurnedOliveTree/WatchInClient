import 'package:watch_in_client/model/api/media/stream/stream.dart';
import 'package:watch_in_client/model/data/channel/channel.dart';
import 'package:watch_in_client/model/data/resources.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/data/media/stream/editable.dart';

class ManageableStream extends EditableStream implements ListableAndEditable {
  @override double length;
  @override DateTime uploaded;
  @override int views;

  ManageableStream({
    required id,
    required title,
    description,
    required this.length,
    required this.uploaded,
    required this.views,
    required status,
    required visibility,
    required uploadUrl,
    required watchUrl,
    required channel,
    required thumbnail,
  }) : super(
    id: id,
    title: title,
    description: description,
    visibility: visibility,
    status: status,
    uploadUrl: uploadUrl,
    watchUrl: watchUrl,
    channel: channel,
    thumbnail: thumbnail,
  );

  factory ManageableStream.fromJson(Map<String, dynamic> json) => ManageableStream(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] == null ? null : json['description'] as String,
    length: json['length'] as double,
    uploaded: DateTime.parse(json['uploaded'] as String),
    views: json['views'] as int,
    visibility: Visibility.fromString(json['visibility'] as String),
    status: Status.streaming,
    uploadUrl: json['uploadUrl'] as String,
    watchUrl: json['watchUrl'] as String,
    channel: Channel.fromJson(json['author']),
    thumbnail: Resource.fromJson(json['thumbnail'] as Map<String, dynamic>),
  );

  @override
  Future<Watchable> toWatchable() async {
    return await StreamService.details(this);
  }
}