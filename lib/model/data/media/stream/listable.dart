import 'package:watch_in_client/model/api/media/stream/stream.dart';
import 'package:watch_in_client/model/data/channel/channel.dart';
import 'package:watch_in_client/model/data/resources.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';

class ListableStream implements Listable {
  @override String id;
  @override String title;
  @override String? description;
  @override Channel channel;
  @override double length;
  @override DateTime uploaded;
  @override int views;
  @override Resource thumbnail;

  ListableStream({
    required this.id,
    required this.title,
    this.description,
    required this.length,
    required this.uploaded,
    required this.views,
    required this.thumbnail,
    required this.channel,
  });

  factory ListableStream.fromJson(Map<String, dynamic> json) => ListableStream(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] == null ? null : json['description'] as String,
    length: json['length'] as double,
    uploaded: DateTime.parse(json['uploaded'] as String),
    views: json['views'] as int,
    thumbnail: Resource.fromJson(json['thumbnail'] as Map<String, dynamic>),
    channel: Channel.fromJson(json['channel'] as Map<String, dynamic>),
  );

  @override
  Future<Watchable> toWatchable() async {
    return await StreamService.details(this);
  }
}