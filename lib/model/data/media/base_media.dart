import 'package:watch_in_client/model/data/channel/channel.dart';
import 'package:watch_in_client/model/data/resources.dart';

enum Visibility {
  private, protected, public;

  @override
  String toString() {
    return name.toUpperCase();
  }

  factory Visibility.fromString(String string) =>
      Visibility.values.byName(string.toLowerCase());
}

enum Status { streaming, processing, partially_ready, ready }

class Likes {
  int likes;
  int dislikes;
  bool liked;
  bool disliked;

  Likes({
    required this.likes,
    required this.dislikes,
    required this.liked,
    required this.disliked,
  });

  factory Likes.fromJson(Map<String, dynamic> json) => Likes(
    likes: json['likes'] as int,
    dislikes: json['dislikes'] as int,
    liked: json['liked'] as bool,
    disliked: json['disliked'] as bool,
  );
}


abstract class BaseMedia {
  abstract String id;
  abstract String title;
  abstract String? description;
}

abstract class UserInfo {
  abstract Channel channel;
  abstract bool favorite;
  abstract bool watchLater;
}

abstract class VideoInfo {
  abstract double length;
  abstract DateTime uploaded;
  abstract int views;
}

abstract class Listable implements BaseMedia, VideoInfo {
  abstract Channel channel;
  abstract Resource thumbnail;

  Future<Watchable> toWatchable();
}

abstract class Watchable implements BaseMedia, UserInfo, VideoInfo {
  abstract Likes likes;
  abstract List<VideoResource>? resources;

  Future<void> view();
}

abstract class Editable implements BaseMedia {
  abstract Visibility visibility;
  abstract Status status;

  Future<void> setTitle(String title);
  Future<void> setDescription(String? description);
  Future<void> setVisibility(Visibility visibility);
  Future<void> setThumbnail(String? path);
  Future<void> delete();
}

abstract class ListableAndEditable implements Editable, Listable { }
