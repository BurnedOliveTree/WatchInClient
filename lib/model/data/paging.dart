class Paging {
  int page;
  int size;

  Paging({required this.page, required this.size});
}
