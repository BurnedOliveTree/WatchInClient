import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/user.dart';
import 'package:watch_in_client/model/state.dart';


class AccountService {
  static Future<User> login(String username, String password, bool rememberMe) async {
    var user = await BaseClient.post(
      uri: '/api/account/login',
      queryParameters: {'username': username, 'password': password, 'remember-me': rememberMe.toString()},
      onReceived: (json) => User.fromJson(json)
    );
    State().user = user;
    return user;
  }

  static logout() async {
    return await BaseClient.post(
      uri: '/api/account/logout',
      onReceived: (json) => State().user = null
    );
  }

  static Future<User> details() async {
    var user = await BaseClient.post(
      uri: '/api/account/details',
      onReceived: (json) => User.fromJson(json)
    );
    State().user = user;
    return user;
  }
}