import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'package:logging/logging.dart';

import 'package:watch_in_client/model/state.dart';
import 'package:watch_in_client/resources/configuration.dart';


enum ContentType {
  json, text, file;

  @override
  String toString() {
    switch (this) {
      case ContentType.json: return 'application/json';
      case ContentType.text: return 'plain/text';
      case ContentType.file: return 'multipart/file';
    }
  }
}

class BaseClient {
  static final log = Logger('WatchInHttpClient');

  static dynamic _decode(String body) {
    return jsonDecode(body);
  }

  static String encode(Object? value) {
    return json.encode(value);
  }

  static Future<T> _request<T>(
    String method,
    String uri,
    Map<String, dynamic>? queryParameters,
    ContentType? contentType,
    T Function(dynamic json) onReceived,
    String? body,
    String? fileName,
    String? filePath,
    { ContentType accept = ContentType.json,
  }) async {
    BaseRequest request;
    if (contentType == ContentType.file) {
      assert (fileName != null);
      assert (filePath != null);
      request = MultipartRequest(method, Uri.http(Configuration.serverURL, uri, queryParameters));
      (request as MultipartRequest).files.add(await MultipartFile.fromPath(fileName!, filePath!));
    } else {
      request = Request(method, Uri.http(Configuration.serverURL, uri, queryParameters));
      if (contentType != null) {
        assert (body != null);
        request.headers.addAll({'Content-Type': contentType.toString()});
        (request as Request).body = body!;
      }
    }
    if (State().cookie != null) {
      request.headers.addAll({'Cookie': '${State().cookie!.name}=${State().cookie!.value}'});
    }
    request.headers.addAll({'Accept': 'application/json'});

    var response = await request.send();
    log.info("Received response from $uri with code ${response.statusCode}");
    if (response.statusCode == 200) {
      if (response.headers['set-cookie'] != null) {
        State().cookie = Cookie.fromSetCookieValue(response.headers['set-cookie']!);
      }
      final body = await response.stream.bytesToString();
      if (body.isNotEmpty) {
        log.finer("Received response body: $body");
        return onReceived(accept == ContentType.json ? _decode(body) : body);
      } else {
        return onReceived(null);
      }
    } else {
      throw HttpException('Failed to $method from $uri.', uri: Uri.http(Configuration.serverURL, uri));
    }
  }

  static Future<T> get<T>({
    required String uri,
    ContentType? contentType,
    required T Function(dynamic json) onReceived,
    Map<String, dynamic>? queryParameters
  }) async {
    return _request('GET', uri, queryParameters, contentType, onReceived, null, null, null);
  }

  static Future<T> post<T>({
    required String uri,
    ContentType? contentType,
    required T Function(dynamic json) onReceived,
    Map<String, dynamic>? queryParameters,
    String? body,
    String? fileName,
    String? filePath,
    ContentType accept = ContentType.json,
  }) async {
    return _request('POST', uri, queryParameters, contentType, onReceived, body, fileName, filePath, accept: accept);
  }

  static Future<T> delete<T>({
    required String uri,
    ContentType? contentType,
    required T Function(dynamic json) onReceived,
    Map<String, dynamic>? queryParameters
  }) async {
    return _request('DELETE', uri, queryParameters, contentType, onReceived, null, null, null);
  }
}