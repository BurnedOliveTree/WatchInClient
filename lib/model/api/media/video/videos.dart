import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/filter.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/data/media/video/listable.dart';
import 'package:watch_in_client/model/data/media/video/manageable.dart';
import 'package:watch_in_client/model/data/paging.dart';


class VideosService {
  static Future<List<ListableVideo>> newest(Paging paging) async {
    return await BaseClient.post(
      uri: '/api/videos/newest',
      contentType: ContentType.json,
      body: BaseClient.encode({"page": paging.page, "size": paging.size}),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ListableVideo.fromJson(video)).toList()
    );
  }

  static Future<List<ListableVideo>> popular(Paging paging) async {
    return await BaseClient.post(
      uri: '/api/videos/popular',
      contentType: ContentType.json,
      body: BaseClient.encode({"page": paging.page, "size": paging.size}),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ListableVideo.fromJson(video)).toList()
    );
  }

  static Future<List<ListableVideo>> favorite(Paging paging) async {
    return await BaseClient.post(
      uri: '/api/videos/favorite',
      contentType: ContentType.json,
      body: BaseClient.encode({"page": paging.page, "size": paging.size}),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ListableVideo.fromJson(video)).toList()
    );
  }

  static Future<List<ListableVideo>> watchLater(Paging paging) async {
    return await BaseClient.post(
      uri: '/api/videos/watch-later',
      contentType: ContentType.json,
      body: BaseClient.encode({"page": paging.page, "size": paging.size}),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ListableVideo.fromJson(video)).toList()
    );
  }

  static Future<List<ListableVideo>> subscribed(Paging paging) async {
    return await BaseClient.post(
      uri: '/api/videos/subscribed',
      contentType: ContentType.json,
      body: BaseClient.encode({"page": paging.page, "size": paging.size}),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ListableVideo.fromJson(video)).toList()
    );
  }

  static Future<List<ListableVideo>> related(Paging paging, BaseMedia video) async {
    return await BaseClient.post(
      uri: '/api/videos/${video.id}/related',
      contentType: ContentType.json,
      body: BaseClient.encode({"page": paging.page, "size": paging.size}),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ListableVideo.fromJson(video)).toList()
    );
  }

  static Future<List<ManageableVideo>> mine(Paging paging, {ManageableFilter? filter}) async {
    return await BaseClient.post(
      uri: '/api/video/manage/list',
      contentType: ContentType.json,
      body: BaseClient.encode({
        "page": paging.page,
        "size": paging.size,
        "filter": {
          "phrase": filter?.phrase,
          "sortField": filter?.sortField,
          "sortDirection": filter?.sortDirection.toString()
        }
      }),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ManageableVideo.fromJson(video)).toList()
    );
  }

  static Future<List<ListableVideo>> search(Paging paging, SearchFilter filter) async {
    return await BaseClient.post(
      uri: '/api/videos/search/filter',
      contentType: ContentType.json,
      body: BaseClient.encode({
        "page": paging.page,
        "size": paging.size,
        "filter": {
          "phrase": filter.phrase,
          "channel": filter.channel,
          "sort": filter.sort?.toString(),
          "date": filter.date?.toString(),
          "duration": filter.duration?.toString(),
        }
      }),
      onReceived: (json) => (json['content']! as Iterable).map((video) => ListableVideo.fromJson(video)).toList()
    );
  }
}