import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/comment.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/data/media/video/watchable.dart';
import 'package:watch_in_client/model/data/paging.dart';


class VideoService {
  static Future<WatchableVideo> details(BaseMedia video) async {
    return await BaseClient.get(
      uri: '/api/video/${video.id}',
      onReceived: (json) => WatchableVideo.fromJson(json)
    );
  }

  static Future<List<Comment>> comments(Paging paging, BaseMedia video) async {
    return await BaseClient.post(
      uri: '/api/video/${video.id}/comments',
      contentType: ContentType.json,
      body: BaseClient.encode({'page': paging.page, 'size': paging.size}),
      onReceived: (json) => (json['content']! as Iterable)
        .map((video) => Comment.fromJson(video))
        .toList(),
    );
  }

  static view(BaseMedia video) async {
    return await BaseClient.post(
      uri: '/api/video/${video.id}/view',
      onReceived: (json) { return; }
    );
  }

  static setFavorite(BaseMedia video, bool favorite) async {
    return await BaseClient.post(
      uri: '/api/video/${video.id}/favorite',
      contentType: ContentType.json,
      body: BaseClient.encode({'removal': !favorite}),
      onReceived: (json) { return; }
    );
  }

  static setWatchLater(BaseMedia video, bool watchLater) async {
    return await BaseClient.post(
      uri: '/api/video/${video.id}/watch-later',
      contentType: ContentType.json,
      body: BaseClient.encode({'removal': !watchLater}),
      onReceived: (json) { return; }
    );
  }

  static Future<void> comment(BaseMedia video, String comment) async {
    return await BaseClient.post(
      uri: '/api/video/${video.id}/comment',
      contentType: ContentType.json,
      body: BaseClient.encode({'content': comment}),
      onReceived: (json) { return; }
    );
  }

  static Future<void> like(BaseMedia video, bool like, bool removal) async {
    return await BaseClient.post(
        uri: '/api/video/${video.id}/like',
        contentType: ContentType.json,
        body: BaseClient.encode({'like': like, 'removal': removal}),
        onReceived: (json) { return; }
    );
  }
}
