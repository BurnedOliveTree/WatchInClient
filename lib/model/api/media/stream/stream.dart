import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/data/media/stream/watchable.dart';


class StreamService {
  static Future<WatchableStream> details(BaseMedia video) async {
    return await BaseClient.get(
        uri: '/api/stream/${video.id}',
        onReceived: (json) => WatchableStream.fromJson(json)
    );
  }

  static view(BaseMedia video) async {
    return await BaseClient.post(
        uri: '/api/stream/${video.id}/view',
        onReceived: (json) { return; }
    );
  }
}
