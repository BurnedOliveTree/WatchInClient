import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/filter.dart';
import 'package:watch_in_client/model/data/media/stream/listable.dart';
import 'package:watch_in_client/model/data/media/stream/manageable.dart';
import 'package:watch_in_client/model/data/paging.dart';


class StreamsService {
  static Future<List<ListableStream>> list(Paging paging) async {
    return await BaseClient.post(
        uri: '/api/streams/list',
        contentType: ContentType.json,
        body: BaseClient.encode({"page": paging.page, "size": paging.size}),
        onReceived: (json) => (json['content']! as Iterable).map((stream) => ListableStream.fromJson(stream)).toList()
    );
  }

  static Future<List<ManageableStream>> mine(Paging paging, {ManageableFilter? filter}) async {
    return await BaseClient.post(
        uri: '/api/stream/manage/list',
        contentType: ContentType.json,
        body: BaseClient.encode({
          "page": paging.page,
          "size": paging.size,
          "filter": {
            "phrase": filter?.phrase,
            "sortField": filter?.sortField,
            "sortDirection": filter?.sortDirection.toString()
          }
        }),
        onReceived: (json) => (json['content']! as Iterable).map((video) => ManageableStream.fromJson(video)).toList()
    );
  }
}
