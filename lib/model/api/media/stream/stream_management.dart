import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/data/media/stream/editable.dart';


class StreamManagementService {
  static Future<EditableStream> create(String title) async {
    return await BaseClient.post(
      uri: '/api/stream/manage',
      contentType: ContentType.text,
      body: title,
      onReceived: (json) => EditableStream.fromJson(json),
    );
  }

  static setTitle(BaseMedia video, String title) async {
    return await BaseClient.post(
        uri: '/api/stream/manage/${video.id}/title',
        contentType: ContentType.text,
        body: title,
        onReceived: (json) { return; }
    );
  }

  static setDescription(BaseMedia video, String description) async {
    return await BaseClient.post(
        uri: '/api/stream/manage/${video.id}/description',
        contentType: ContentType.text,
        body: description,
        onReceived: (json) { return; }
    );
  }

  static deleteDescription(BaseMedia video) async {
    return await BaseClient.delete(
        uri: '/api/stream/manage/${video.id}/description',
        onReceived: (json) { return; }
    );
  }

  static setVisibility(BaseMedia video, Visibility visibility) async {
    return await BaseClient.post(
        uri: '/api/stream/manage/${video.id}/visibility',
        contentType: ContentType.text,
        body: visibility.toString(),
        onReceived: (text) { return; }
    );
  }

  static setThumbnail(BaseMedia video, String path) async {
    return await BaseClient.post(
        uri: '/api/stream/manage/${video.id}/thumbnail',
        contentType: ContentType.file,
        fileName: 'thumbnail',
        filePath: path,
        onReceived: (text) { return; }
    );
  }

  static deleteThumbnail(BaseMedia video) async {
    return await BaseClient.delete(
        uri: '/api/stream/manage/${video.id}/thumbnail',
        onReceived: (text) { return; }
    );
  }

  static delete(BaseMedia video) async {
    return await BaseClient.delete(
        uri: '/api/stream/manage/${video.id}',
        onReceived: (json) { return; }
    );
  }
}