import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/channel/channel_stats.dart';


class DashboardService {
  static Future<ChannelStatistics> statistics() async {
    final stats = await BaseClient.get(
      uri: '/api/dashboard/statistics',
      onReceived: (body) => ChannelStatistics.fromJson(body),
    );
    return stats;
  }
}
