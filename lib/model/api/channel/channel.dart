import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/channel/channel_details.dart';


class ChannelService {
  static Future<ChannelDetails> details(String username) async {
    return await BaseClient.get(
      uri: '/api/channel/$username',
      onReceived: (json) => ChannelDetails.fromJson(json),
    );
  }

  static Future<bool> subscription(String username) async {
    return await BaseClient.get(
      uri: '/api/channel/$username/subscription',
      onReceived: (json) { return json['subscribed'] as bool; },
    );
  }

  static Future<bool> setSubscription(String username, bool status) async {
    return await BaseClient.post(
      uri: '/api/channel/$username/subscribe',
      contentType: ContentType.json,
      body: BaseClient.encode({'removal': !status}),
      onReceived: (json) { return json['subscribed'] as bool; },
    );
  }
}
