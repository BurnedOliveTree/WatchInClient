import 'package:watch_in_client/model/api/base_client.dart';
import 'package:watch_in_client/model/data/channel/my_channel.dart';
import 'package:watch_in_client/model/data/resources.dart';


class ChannelManagementService {
  static Future<MyChannel> details() async {
    return await BaseClient.get(
      uri: '/api/channel/manage/edit',
      onReceived: (json) => MyChannel.fromJson(json),
    );
  }

  static Future<Resource> setAvatar(String path) async {
    return await BaseClient.post(
        uri: '/api/channel/manage/avatar',
        contentType: ContentType.file,
        fileName: 'avatar',
        filePath: path,
        onReceived: (json) => Resource.fromJson(json),
    );
  }

  static Future<Resource> resetAvatar() async {
    return await BaseClient.delete(
      uri: '/api/channel/manage/avatar',
      onReceived: (json) => Resource.fromJson(json)
    );
  }

  static Future<Resource> setBackground(String path) async {
    return await BaseClient.post(
      uri: '/api/channel/manage/background',
      contentType: ContentType.file,
      fileName: 'background',
      filePath: path,
      onReceived: (json) => Resource.fromJson(json),
    );
  }

  static Future<Resource> resetBackground() async {
    return await BaseClient.delete(
        uri: '/api/channel/manage/background',
        onReceived: (json) => Resource.fromJson(json)
    );
  }

  static setDescription(String description) async {
    return await BaseClient.post(
      uri: '/api/channel/manage/description',
      contentType: ContentType.text,
      onReceived: (json) { return; },
      body: description
    );
  }

  static resetDescription() async {
    return await BaseClient.delete(
      uri: '/api/channel/manage/description',
      onReceived: (json) { return; },
    );
  }
}