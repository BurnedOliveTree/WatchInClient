import 'package:flutter/material.dart';

class Themes {
  static MaterialColor colour = Colors.red;

  static ThemeData lightTheme = ThemeData(
    primaryColor: colour.shade600,
    backgroundColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    colorScheme: const ColorScheme.light(),
    appBarTheme: AppBarTheme(
      backgroundColor: colour.shade600,
    ),
    iconTheme: IconThemeData(
      color: colour.shade600,
      size: 32.0
    ),
    listTileTheme: ListTileThemeData(
      iconColor: colour.shade600
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      foregroundColor: Colors.black,
      backgroundColor: colour.shade600
    )
  );

  static ThemeData darkTheme = ThemeData(
    primaryColor: colour.shade900,
    backgroundColor: Colors.black38,
    scaffoldBackgroundColor: Colors.black38,
    colorScheme: const ColorScheme.dark(),
    appBarTheme: AppBarTheme(
      backgroundColor: colour.shade900,
    ),
    iconTheme: const IconThemeData(
      color: Colors.white,
      size: 32.0
    ),
    listTileTheme: const ListTileThemeData(
      iconColor: Colors.white,
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      foregroundColor: Colors.white,
      backgroundColor: colour.shade900,
    )
  );
}