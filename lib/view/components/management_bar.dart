import 'package:flutter/material.dart';
import 'package:watch_in_client/model/api/media/video/video_management.dart';
import 'package:watch_in_client/model/state.dart' as watch_in;
import 'package:watch_in_client/view/components/create_stream_button.dart';
import 'package:watch_in_client/view/components/upload_media_button.dart';
import 'package:watch_in_client/view/pages/management/dashboard.dart';
import 'package:watch_in_client/view/pages/channel.dart';
import 'package:watch_in_client/view/pages/management/my_media.dart';


AppBar managementBar(BuildContext context) {
  return AppBar(
    title: const Text("Dashboard"),
    actions: [
      IconButton(
          icon: const Icon(Icons.home_filled),
          tooltip: "My channel",
          onPressed: () {
            Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => ChannelPage(user: watch_in.State().user!.name),
            ));
          }
      ),
      IconButton(
          icon: const Icon(Icons.multiline_chart),
          tooltip: "Dashboard",
          onPressed: () {
            Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => const DashboardPage(),
            ));
          }
      ),
      const CreateStreamButton(),
      UploadMediaButton(
        tooltip: "Upload video",
        onFileReceive: (path) async { await VideoManagementService.upload(path); },
      ),
      IconButton(
          icon: const Icon(Icons.video_collection),
          tooltip: "My videos",
          onPressed: () {
            Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => const MyMediaPage(),
            ));
          }
      ),
    ],
  );
}