import 'package:flutter/material.dart';
import 'package:watch_in_client/view/components/search_pop_up.dart';


class SearchButton extends StatefulWidget {
  const SearchButton({required this.setState, Key? key}) : super(key: key);

  final Function(String) setState;

  @override
  SearchButtonState createState() => SearchButtonState();
}

class SearchButtonState extends State<SearchButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.search),
      tooltip: "Search",
      onPressed: () async {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return SearchPopUp(key: widget.key);
          }
        ).then((content) async {
          if (content != null) {
            widget.setState(content);
          }
        });
      }
    );
  }
}