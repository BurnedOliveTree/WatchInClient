import 'package:flutter/material.dart';


class SearchPopUp extends StatefulWidget {
  const SearchPopUp({Key? key}) : super(key: key);

  @override
  SearchPopUpState createState() => SearchPopUpState();
}

class SearchPopUpState extends State<SearchPopUp> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Search"),
      content: TextField(
        autofocus: true,
        onSubmitted: (value) { Navigator.pop(context, value); },
        decoration: const InputDecoration(hintText: "Your search prompt here"),
      ),
    );
  }
}