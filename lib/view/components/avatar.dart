import 'package:flutter/material.dart';
import 'package:watch_in_client/model/state.dart' as watch_in;

Widget avatar() {
  String? avatarURL = watch_in.State().user?.avatar?.url;
  if (avatarURL != null) {
    return CircleAvatar(backgroundImage: NetworkImage(avatarURL));
  } else {
    return const Icon(Icons.person);
  }
}