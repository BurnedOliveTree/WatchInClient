import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:logging/logging.dart';

class ThumbnailWidget extends StatefulWidget {
  const ThumbnailWidget(
      {Key? key,
        required this.source,
        required this.selected,
        required this.onTap})
      : super(key: key);
  final DesktopCapturerSource source;
  final bool selected;
  final Function(DesktopCapturerSource) onTap;

  @override
  ThumbnailWidgetState createState() => ThumbnailWidgetState();
}

class ThumbnailWidgetState extends State<ThumbnailWidget> {
  final List<StreamSubscription> _subscriptions = [];

  @override
  void initState() {
    super.initState();
    _subscriptions.add(widget.source.onThumbnailChanged.stream.listen((event) {
      setState(() {});
    }));
    _subscriptions.add(widget.source.onNameChanged.stream.listen((event) {
      setState(() {});
    }));
  }

  @override
  void deactivate() {
    for (var element in _subscriptions) {
      element.cancel();
    }
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Container(
            decoration: widget.selected
              ? BoxDecoration(border: Border.all(
                width: 2,
                color: Theme.of(context).colorScheme.secondary))
              : null,
            child: InkWell(
              onTap: () {
                widget.onTap(widget.source);
              },
              child: widget.source.thumbnail != null
                ? Image.memory(
                  widget.source.thumbnail!,
                  gaplessPlayback: true,
                  alignment: Alignment.center,)
                : Container(),
            ),
          )),
        Text(
          widget.source.name,
          style: TextStyle(fontWeight: widget.selected ? FontWeight.bold : FontWeight.normal),
        ),
      ],
    );
  }
}

class ScreenSelectState {
  static final log = Logger('WatchInScreenSelectController');
  final List<StreamSubscription<DesktopCapturerSource>> _subscriptions = [];
  final Map<String, DesktopCapturerSource> _sources = {};
  SourceType _sourceType = SourceType.Screen;
  DesktopCapturerSource? selectedSource;
  StateSetter? stateSetter;
  Timer? _timer;

  ScreenSelectState() {
    refresh();
    _subscriptions.add(desktopCapturer.onAdded.stream.listen((source) {
      _sources[source.id] = source;
      stateSetter?.call(() {});
    }));
    _subscriptions.add(desktopCapturer.onRemoved.stream.listen((source) {
      _sources.remove(source.id);
      stateSetter?.call(() {});
    }));
    _subscriptions.add(desktopCapturer.onThumbnailChanged.stream.listen((source) {
      stateSetter?.call(() {});
    }));
  }

  Future<void> refresh() async {
    try {
      var newSources = await desktopCapturer.getSources(types: [_sourceType]);
      _timer?.cancel();
      _timer = Timer.periodic(const Duration(seconds: 3), (timer) {
        desktopCapturer.updateSources(types: [_sourceType]);
      });
      _sources.clear();
      for (var element in newSources) {
        _sources[element.id] = element;
      }
      stateSetter?.call(() {});
    } catch (e) {
      log.warning(e.toString());
    }
  }

  Iterable<DesktopCapturerSource> getSources() {
    return _sources.values;
  }

  void setType(SourceType newType) {
    _sourceType = newType;
  }

  void close() {
    _timer?.cancel();
    for (var element in _subscriptions) {
      element.cancel();
    }
  }
}

class ScreenSelectDialog extends Dialog {
  final ScreenSelectState state = ScreenSelectState();

  ScreenSelectDialog({super.key});

  void _ok(context) async {
    state.close();
    Navigator.pop<DesktopCapturerSource>(context, state.selectedSource);
  }

  void _cancel(context) async {
    state.close();
    Navigator.pop<DesktopCapturerSource>(context, null);
  }

  Widget _tab(SourceType sourceType, Function setState) {
    return Center(child: GridView.count(
      crossAxisSpacing: 8,
      crossAxisCount: 3,
      children: state.getSources()
        .where((source) => source.type == sourceType)
        .map((source) => ThumbnailWidget(
          source: source,
          selected: state.selectedSource?.id == source.id,
          onTap: (selected) {
            setState(() {
              state.selectedSource = selected;
            });
          },
        )).toList(),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(64),
      child: Material(
        type: MaterialType.card,
        child: Center(child: Column(children: <Widget>[
          const Padding(
            padding: EdgeInsets.all(10),
            child: Text('Choose what to share'),
          ),
          Expanded(
            flex: 1,
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.all(10),
              child: StatefulBuilder(
                builder: (context, setState) {
                  state.stateSetter = setState;
                  return DefaultTabController(
                    length: 2,
                    child: Column(children: <Widget>[
                      Container(
                        constraints: const BoxConstraints.expand(height: 24),
                        child: TabBar(
                          onTap: (value) {
                            state.setType(value == 0 ? SourceType.Screen : SourceType.Window);
                            state.refresh();
                          },
                          tabs: const [
                            Tab(child: Text('Entire Screen')),
                            Tab(child: Text('Window')),
                        ]),
                      ),
                      const SizedBox(height: 2),
                      Expanded(child: TabBarView(children: [
                        _tab(SourceType.Screen, setState),
                        _tab(SourceType.Window, setState),
                      ]))
                    ]),
                  );
                },
              ),
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: ButtonBar(children: <Widget>[
              MaterialButton(
                child: const Text('Cancel'),
                onPressed: () { _cancel(context); },
              ),
              MaterialButton(
                color: Theme.of(context).primaryColor,
                child: const Text('Share'),
                onPressed: () { _ok(context); },
              ),
            ]),
          ),
        ])),
      ),
    );
  }
}