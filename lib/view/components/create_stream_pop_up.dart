import 'package:flutter/material.dart';


class CreateStreamPopUp extends StatefulWidget {
  const CreateStreamPopUp({Key? key}) : super(key: key);

  @override
  CreateStreamPopUpState createState() => CreateStreamPopUpState();
}

class CreateStreamPopUpState extends State<CreateStreamPopUp> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Title your stream"),
      content: TextField(
        onSubmitted: (value) { Navigator.pop(context, value); },
        decoration: const InputDecoration(hintText: "Title"),
      ),
    );
  }
}