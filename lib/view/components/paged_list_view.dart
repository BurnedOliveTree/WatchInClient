import 'package:flutter/material.dart';
import 'package:watch_in_client/model/data/paging.dart';

class PagedListView<T> extends StatefulWidget {
  const PagedListView({
    required this.futureFunction,
    this.headerBuilder,
    required this.itemBuilder,
    required this.emptyText,
    this.resetOn,
    Key? key}) : super(key: key);

  final Future<List<T>> Function(Paging) futureFunction;
  final Widget Function()? headerBuilder;
  final Widget Function(T) itemBuilder;
  final String emptyText;
  final ChangeNotifier? resetOn;

  @override
  PagedListViewState<T> createState() => PagedListViewState<T>();
}

class PagedListViewState<T> extends State<PagedListView<T>> {
  List<T> items = [];
  Paging paging = Paging(page: 0, size: 10);
  bool end = false;

  _requestForMoreItems() async {
    final moreItems = await widget.futureFunction(paging);
    if (moreItems.isEmpty) {
      setState(() => end = true);
    } else {
      setState(() {
        items.addAll(moreItems);
        paging.page += 1;
      });
    }
  }

  int _oneForHeader() => widget.headerBuilder == null ? 0 : 1;

  void _resetState() {
    items = [];
    paging = Paging(page: 0, size: 10);
    end = false;
  }

  @override
  void initState() {
    widget.resetOn?.addListener(() => _resetState());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const ClampingScrollPhysics(),
      itemCount: items.length + 1 + _oneForHeader(),
      itemBuilder: (context, index) {
        if (widget.headerBuilder != null && index == 0) {
          return widget.headerBuilder!();
        } else if (index < items.length + _oneForHeader()) {
          return widget.itemBuilder(items[index - _oneForHeader()]);
        } else if (end) {
          if (items.isEmpty) {
            return Center(child: Text(widget.emptyText));
          } else {
            return Container();
          }
        } else {
          _requestForMoreItems();
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}