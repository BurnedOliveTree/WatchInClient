import 'package:flutter/material.dart';
import 'package:watch_in_client/model/data/media/base_media.dart' as model;


class VisibilityPicker extends StatefulWidget {
  const VisibilityPicker({required this.media, super.key});

  final model.Editable media;

  @override
  VisibilityPickerState createState() => VisibilityPickerState();
}

class VisibilityPickerState extends State<VisibilityPicker> {
  model.Visibility visibility = model.Visibility.private;

  @override
  Widget build(BuildContext context) {
    visibility = widget.media.visibility;
    return DropdownButton<model.Visibility>(
      value: visibility,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      onChanged: (model.Visibility? value) {
        setState(() {
          visibility = value!;
          widget.media.setVisibility(visibility);
        });
      },
      items: model.Visibility.values.map<DropdownMenuItem<model.Visibility>>((model.Visibility value) {
        return DropdownMenuItem<model.Visibility>(
          value: value,
          child: Text(value.name),
        );
      }).toList(),
    );
  }
}