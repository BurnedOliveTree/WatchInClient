import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:watch_in_client/model/utilities/datetime.dart';


SizedBox standardChart(BuildContext context, {required Iterable<MapEntry<DateTime, int>> data, required String title}) {
  return SizedBox(
    height: 200,
    child: SfCartesianChart(
      title: ChartTitle(text: title),
      tooltipBehavior: TooltipBehavior(enable: true),
      palette: [Theme.of(context).primaryColor],
      primaryXAxis: CategoryAxis(),
      series: [
        LineSeries(
          width: 3.0,
          markerSettings: const MarkerSettings(isVisible: true),
          dataSource: data.toList(),
          xValueMapper: (MapEntry data, _) => formDate(data.key),
          yValueMapper: (MapEntry data, _) => data.value,
        ),
        AreaSeries(
          opacity: 0.5,
          dataSource: data.toList(),
          xValueMapper: (MapEntry data, _) => formDate(data.key),
          yValueMapper: (MapEntry data, _) => data.value,
        )
      ],
    ),
  );
}