import 'package:flutter/material.dart';
import 'package:watch_in_client/model/api/media/stream/stream_management.dart';
import 'package:watch_in_client/view/components/create_stream_pop_up.dart';
import 'package:watch_in_client/view/components/created_stream_pop_up.dart';


class CreateStreamButton extends StatefulWidget {
  const CreateStreamButton({Key? key}) : super(key: key);

  @override
  CreateStreamButtonState createState() => CreateStreamButtonState();
}

class CreateStreamButtonState extends State<CreateStreamButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.video_call_rounded),
      tooltip: "Create stream",
      onPressed: () async {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return CreateStreamPopUp(key: widget.key);
          }
        ).then((title) async {
          if (title != null) {
            final media = await StreamManagementService.create(title);
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return CreatedStreamPopUp(media: media, key: widget.key);
              }
            );
          }
        });
      }
    );
  }
}