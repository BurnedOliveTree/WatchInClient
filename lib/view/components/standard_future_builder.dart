import 'package:flutter/material.dart';
import 'package:logging/logging.dart';


FutureBuilder<T> defaultFutureBuilder<T>({
  required Future<T> Function() futureFunction,
  required Widget Function(T) child
}) {
  final log = Logger("DefaultFutureBuilder");

  return FutureBuilder<T>(
    future: futureFunction(),
    builder: (context, snapshot) {
      if (snapshot.hasError) {
        log.severe(snapshot.error);
        return const Text("Something went wrong :(");
      } else if (!snapshot.hasData) {
        return const SizedBox(child: CircularProgressIndicator());
      } else {
        return child(snapshot.data!);
      }
    },
  );
}