import 'package:flutter/material.dart';
import 'package:watch_in_client/model/data/media/stream/editable.dart';


class CreatedStreamPopUp extends StatefulWidget {
  const CreatedStreamPopUp({required this.media, Key? key}) : super(key: key);

  final EditableStream media;

  @override
  CreatedStreamPopUpState createState() => CreatedStreamPopUpState();
}

class CreatedStreamPopUpState extends State<CreatedStreamPopUp> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Stream \"${widget.media.title}\" has been created!"),
      content: Column(children: [
        SelectableText("Stream key: ${widget.media.id}"),
        SelectableText("Upload URL: ${widget.media.uploadUrl}"),
      ])
    );
  }
}