import 'package:flutter/material.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/utilities/datetime.dart';


Widget videoTile({required Listable media, required void Function() onTap}) {
  return Card(
    child: InkWell(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.all(4.0),
        child: Column(
          children: [
            Stack(
              alignment: Alignment.bottomRight,
              children: [
                Image.network(media.thumbnail.url),
                Container(
                  padding: const EdgeInsets.all(4.0),
                  decoration: const BoxDecoration(
                    color: Colors.black,
                    backgroundBlendMode: BlendMode.darken
                  ),
                  child: Text(formTime(media.length),
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ]
            ),
            ListTile(
              leading: CircleAvatar(backgroundImage: NetworkImage(media.channel.avatar.url)),
              title: Text(media.title, overflow: TextOverflow.ellipsis),
              subtitle: Text("${media.views} views\n${formDate(media.uploaded)}"),
              isThreeLine: true,
            ),
          ],
        ),
      ),
    ),
  );
}
