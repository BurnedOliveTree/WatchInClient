import 'package:flutter/material.dart';
import 'package:watch_in_client/view/components/upload_media_pop_up.dart';


class UploadMediaButton extends StatefulWidget {
  const UploadMediaButton({
    required this.tooltip,
    required this.onFileReceive,
    Key? key}) : super(key: key);

  final String tooltip;
  final Function(String) onFileReceive;

  @override
  UploadMediaButtonState createState() => UploadMediaButtonState();
}

class UploadMediaButtonState extends State<UploadMediaButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: const Icon(Icons.upload),
        tooltip: widget.tooltip,
        onPressed: () async {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return UploadMediaPopUp(key: widget.key);
              }
          ).then((paths) async {
            if (paths != null) {
              widget.onFileReceive(paths.first);
            }
          });
        }
    );
  }
}