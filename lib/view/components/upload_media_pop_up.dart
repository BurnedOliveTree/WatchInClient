import 'package:desktop_drop/desktop_drop.dart';
import 'package:flutter/material.dart';


class UploadMediaPopUp extends StatefulWidget {
  const UploadMediaPopUp({Key? key}) : super(key: key);

  @override
  UploadMediaPopUpState createState() => UploadMediaPopUpState();
}

class UploadMediaPopUpState extends State<UploadMediaPopUp> {
  bool _dragging = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Drop media file(s) below"),
      actions: [
        DropTarget(
          onDragDone: (detail) {
            Navigator.of(context).pop(detail.files.map((file) => file.path).toSet());
          },
          onDragEntered: (detail) {
            setState(() {
              _dragging = true;
            });
          },
          onDragExited: (detail) {
            setState(() {
              _dragging = false;
            });
          },
          child: Container(
            height: 256,
            color: _dragging
              ? Theme.of(context).colorScheme.secondary.withOpacity(0.4)
              : Theme.of(context).colorScheme.background,
            child: const Center(child: Text("Drop here")),
          ),
        )
      ],
    );
  }
}