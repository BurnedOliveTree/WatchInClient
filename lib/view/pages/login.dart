import 'package:flutter/material.dart';
import 'package:watch_in_client/model/api/account.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String? username;
  String? password;

  login() async {
    if (username == null || password == null) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Username or password missing!"),
            actions: [
              TextButton(
                child: const Text("Okay"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    } else {
      final user = await AccountService.login(username!, password!, false);
      Navigator.of(context).pop(user);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Login")),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Text('Username'),
          TextField(
            onChanged: (value) { username = value; },
            keyboardType: TextInputType.text,
            textAlign: TextAlign.center,
            autofocus: true,
          ),
          const SizedBox(height: 8.0),
          const Text('Password'),
          TextField(
            onChanged: (value) { password = value; },
            keyboardType: TextInputType.text,
            textAlign: TextAlign.center,
            obscureText: true,
          ),
          const SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(
                child: const Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: const Text("Proceed"),
                onPressed: login,
              ),
            ],
          ),
          const SizedBox(height: 8.0),
          const Text('Registration available at the website', style: TextStyle(color: Colors.grey))
        ],
      ),
    );
  }
}