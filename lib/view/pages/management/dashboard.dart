import 'package:flutter/material.dart';

import 'package:watch_in_client/model/api/dashboard.dart';
import 'package:watch_in_client/model/data/channel/channel_stats.dart';
import 'package:watch_in_client/view/components/management_bar.dart';
import 'package:watch_in_client/view/components/standard_chart.dart';
import 'package:watch_in_client/view/components/standard_future_builder.dart';


class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: managementBar(context),
      body: Center(
        child: defaultFutureBuilder<ChannelStatistics>(
          futureFunction: DashboardService.statistics,
          child: (statistics) => ListView(children: [
            standardChart(context,
              data: statistics.stats.map((stat) => MapEntry(stat.date, stat.subscribers)),
              title: 'Amount of subscriptions to your channel'
            ),
            standardChart(context,
              data: statistics.stats.map((stat) => MapEntry(stat.date, stat.videos)),
              title: 'Amount of your videos'
            ),
            standardChart(context,
              data: statistics.stats.map((stat) => MapEntry(stat.date, stat.views)),
              title: 'Amount of views of your videos'
            ),
            standardChart(context,
              data: statistics.stats.map((stat) => MapEntry(stat.date, stat.comments)),
              title: 'Amount of comments under your videos'
            ),
          ])
        )
      ),
    );
  }
}
