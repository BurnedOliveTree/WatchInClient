import 'package:flutter/material.dart';
import 'package:watch_in_client/model/api/media/stream/streams.dart';
import 'package:watch_in_client/model/api/media/video/videos.dart';
import 'package:watch_in_client/model/data/filter.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/utilities/datetime.dart';
import 'package:watch_in_client/view/components/management_bar.dart';
import 'package:watch_in_client/view/components/paged_list_view.dart';
import 'package:watch_in_client/view/components/upload_media_button.dart';
import 'package:watch_in_client/view/components/visibility_picker.dart';
import 'package:watch_in_client/view/pages/media.dart';


class MyMediaPage extends StatefulWidget {
  const MyMediaPage({Key? key}) : super(key: key);

  @override
  State<MyMediaPage> createState() => _MyMediaPageState();
}

class Notifier extends ChangeNotifier {
  void notify() => notifyListeners();
}

class _MyMediaPageState extends State<MyMediaPage> {
  ManageableFilter? _filter;
  final Notifier notifier = Notifier();

  Tooltip statusToIcon(Status status) {
    switch (status) {
      case Status.streaming:
        return const Tooltip(
          message: "Streaming",
          child: Icon(Icons.video_camera_front),
        );
      case Status.processing:
        return const Tooltip(
          message: "Processing",
          child: Icon(Icons.motion_photos_on_outlined),
        );
      case Status.partially_ready:
        return const Tooltip(
          message: "Partially ready",
          child: Icon(Icons.motion_photos_on),
        );
      case Status.ready:
        return const Tooltip(
          message: "Ready",
          child: Icon(Icons.check_circle),
        );
    }
  }

  Widget withLabel({required String label, required Widget child}) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
      child: Row(children: [
        Flexible(fit: FlexFit.tight, flex: 1, child: Text(label)),
        Flexible(fit: FlexFit.tight, flex: 3, child: child),
      ])
    );
  }

  Widget withLabelAndImage({required String label, required Image image, required Widget child}) {
    return Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Row(children: [
          Flexible(fit: FlexFit.tight, flex: 1, child: Text(label)),
          Flexible(fit: FlexFit.tight, flex: 2, child: image),
          Flexible(fit: FlexFit.tight, flex: 1, child: child),
        ])
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: managementBar(context),
      body: Center(child: Column(children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: TextField(
            autofocus: true,
            decoration: const InputDecoration(icon: Icon(Icons.search), hintText: "Search by name"),
            onSubmitted: (value) {
              setState(() {
                if (value.isEmpty) {
                  _filter = null;
                } else {
                  _filter = ManageableFilter(phrase: value);
                }
                notifier.notify();
              });
            },
          ),
        ),
        const SizedBox(height: 8.0),
        PagedListView<ListableAndEditable>(
          resetOn: notifier,
          futureFunction: (paging) async {
            var media = await Future.wait([VideosService.mine(paging, filter: _filter), StreamsService.mine(paging, filter: _filter)]);
            return media.expand((i) => i).toList();
          },
          headerBuilder: () => Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: const [
            Flexible(fit: FlexFit.tight, child: Text("Title", style: TextStyle(fontWeight: FontWeight.bold))),
            Flexible(fit: FlexFit.tight, child: Text("Created at", style: TextStyle(fontWeight: FontWeight.bold))),
            Flexible(fit: FlexFit.tight, child: Text("Length", style: TextStyle(fontWeight: FontWeight.bold))),
            Flexible(fit: FlexFit.tight, child: Text("Visibility", style: TextStyle(fontWeight: FontWeight.bold))),
            Flexible(fit: FlexFit.tight, child: Text("Status", style: TextStyle(fontWeight: FontWeight.bold))),
            Flexible(fit: FlexFit.loose, child: Text("Play", style: TextStyle(fontWeight: FontWeight.bold))),
            Flexible(fit: FlexFit.loose, child: Text("Delete", style: TextStyle(fontWeight: FontWeight.bold))),
            Flexible(fit: FlexFit.loose, child: Text("Edit", style: TextStyle(fontWeight: FontWeight.bold))),
          ]),
          itemBuilder: (manageable) => ExpansionTile(
            title: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Flexible(fit: FlexFit.tight, child: Text(manageable.title)),
                  Flexible(fit: FlexFit.tight, child: Text(formDate(manageable.uploaded))),
                  Flexible(fit: FlexFit.tight, child: Text(formTime(manageable.length))),
                  Flexible(fit: FlexFit.tight, child: Text("${manageable.visibility}")),
                  Flexible(fit: FlexFit.tight, child: statusToIcon(manageable.status)),
                  Flexible(fit: FlexFit.loose, child: IconButton(
                      onPressed: () async {
                        final video = await manageable.toWatchable();
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => MediaPage(media: video, isLoggedIn: true)
                        ));
                      },
                      icon: const Icon(Icons.play_arrow)
                  )),
                  Flexible(fit: FlexFit.loose, child: IconButton(
                      onPressed: () => manageable.delete(),
                      icon: const Icon(Icons.delete)
                  ))
                ]),
            tilePadding: const EdgeInsets.all(10),
            children: [
              withLabel(label: "Title", child: TextFormField(
                initialValue: manageable.title,
                onFieldSubmitted: (value) => manageable.setTitle(value),
              )),
              withLabel(label: "Description", child: TextFormField(
                initialValue: manageable.description,
                onFieldSubmitted: (value) => manageable.setDescription(value),
              )),
              withLabel(label: "Visibility", child: VisibilityPicker(media: manageable)),
              withLabelAndImage(
                label: "Thumbnail",
                image: Image(image: NetworkImage(manageable.thumbnail.url)),
                child: UploadMediaButton(
                  tooltip: "Upload thumbnail",
                  onFileReceive: (path) async => await manageable.setThumbnail(path),
                ),
              ),
            ],
          ),
          emptyText: "You haven't uploaded any media yet"
        )
      ])),
    );
  }
}
