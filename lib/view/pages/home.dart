import 'package:flutter/material.dart';

import 'package:watch_in_client/model/api/account.dart';
import 'package:watch_in_client/model/api/media/stream/streams.dart';
import 'package:watch_in_client/model/api/media/video/videos.dart';
import 'package:watch_in_client/model/data/filter.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/data/paging.dart';
import 'package:watch_in_client/model/state.dart' as watch_in;
import 'package:watch_in_client/view/components/avatar.dart';
import 'package:watch_in_client/view/components/paged_grid_view.dart';
import 'package:watch_in_client/view/components/search_button.dart';
import 'package:watch_in_client/view/components/video_tile.dart';
import 'package:watch_in_client/view/pages/login.dart';
import 'package:watch_in_client/view/pages/channel.dart';
import 'package:watch_in_client/view/pages/media.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class Notifier extends ChangeNotifier {
  void notify() => notifyListeners();
}

class _HomePageState extends State<HomePage> {
  bool _isLoggedIn = false;
  Future<List<Listable>> Function(Paging) _selection = VideosService.newest;
  Future<List<Listable>> Function(Paging) _search = VideosService.newest;
  Notifier notifier = Notifier();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset("lib/resources/assets/logo.png", scale: 4.0),
        actions: [
          DropdownButton<Future<List<Listable>> Function(Paging)>(
            value: _selection,
            icon: const Icon(Icons.keyboard_arrow_down),
            iconSize: Theme.of(context).iconTheme.size ?? 24.0,
            underline: Container(height: 0),
            onChanged: (value) {
              if (value != null) {
                setState(() {
                  _selection = value;
                  _search = _selection;
                  notifier.notify();
                });
              }
            },
            items: [
              const DropdownMenuItem(value: VideosService.newest, child: Text("Newest")),
              const DropdownMenuItem(value: VideosService.popular, child: Text("Popular")),
              const DropdownMenuItem(value: StreamsService.list, child: Text("Live streams")),
              if (_isLoggedIn)
                const DropdownMenuItem(value: VideosService.favorite, child: Text("Favorite")),
              if (_isLoggedIn)
                const DropdownMenuItem(value: VideosService.watchLater, child: Text("Watchlist")),
              if (_isLoggedIn)
                const DropdownMenuItem(value: VideosService.subscribed, child: Text("Subscribed")),
            ]
          ),
          SearchButton(setState: (content) => { setState(() {
            _search = (paging) => VideosService.search(paging, SearchFilter(phrase: content, sort: SortOrder.relevance));
            notifier.notify();
          })}),
          if (!_isLoggedIn)
            IconButton(
              icon: const Icon(Icons.login),
              tooltip: "Login",
              onPressed: () async {
                final user = await Navigator.push(context, MaterialPageRoute(
                  builder: (context) => const LoginPage(),
                ));
                if (user != null) {
                  setState(() {
                    _isLoggedIn = true;
                  });
                }
              }
            )
          else
            IconButton(
              icon: const Icon(Icons.logout),
              tooltip: "Logout",
              onPressed: () async {
                await AccountService.logout();
                setState(() {
                  _isLoggedIn = false;
                });
              }),
          if (_isLoggedIn)
            IconButton(
              icon: avatar(),
              tooltip: "My channel",
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => ChannelPage(user: watch_in.State().user!.name),
                ));
              }
            ),
        ],
      ),
      body: Center(
        child: PagedGridView<Listable>(
          resetOn: notifier,
          futureFunction: (paging) => _search(paging),
          itemBuilder: (listable) => videoTile(
            media: listable,
            onTap: () async {
              final media = await listable.toWatchable();
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => MediaPage(media: media, isLoggedIn: _isLoggedIn),
              ));
            },
          ),
          emptyText: "No videos in this section",
        )
      ),
    );
  }
}
