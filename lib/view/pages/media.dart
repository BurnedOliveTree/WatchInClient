import 'package:watch_in_client/model/api/channel/channel.dart';
import 'package:watch_in_client/model/api/media/video/video.dart';
import 'package:watch_in_client/model/api/media/video/videos.dart';
import 'package:watch_in_client/model/data/comment.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/state.dart' as watch_in;

import 'package:flutter/material.dart';
import 'package:mpv_dart/mpv_dart.dart';
import 'package:watch_in_client/model/utilities/datetime.dart';
import 'package:watch_in_client/view/components/avatar.dart';
import 'package:watch_in_client/view/components/paged_list_view.dart';
import 'package:watch_in_client/view/components/standard_future_builder.dart';
import 'package:watch_in_client/view/components/video_list.dart';
import 'package:watch_in_client/view/pages/channel.dart';


class MediaPage extends StatefulWidget {
  const MediaPage({
    required this.media,
    required this.isLoggedIn,
    Key? key}) : super(key: key);

  final Watchable media;
  final bool isLoggedIn;

  @override
  State<MediaPage> createState() => _MediaPageState();
}

class _MediaPageState extends State<MediaPage> {
  MPVPlayer player = MPVPlayer();
  bool _subscription = false;
  bool _favorite = false;
  bool _liked = false;
  bool _disliked = false;

  Future<bool> load() async {
    if (!player.isRunning()) {
      await player.start();
      await player.load(widget.media.resources![0].url);
      widget.media.view();
    }
    _subscription = await ChannelService.subscription(widget.media.channel.name);
    _favorite = widget.media.favorite;
    _liked = widget.media.likes.liked;
    _disliked = widget.media.likes.disliked;
    return player.isRunning();
  }

  @override
  void dispose() {
    super.dispose();
    player.quit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.media.title)
      ),
      body: Center(
        child: defaultFutureBuilder<void>(
          futureFunction: load,
          child: (snapshot) {
            return ListView(padding: const EdgeInsets.all(10.0), children: [
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))
                ),
                child: const AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Center(child: Text("Video opened in a second window"))
                ),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Flexible(flex: 4, fit: FlexFit.tight, child: ListTile(
                  leading: CircleAvatar(backgroundImage: NetworkImage(widget.media.channel.avatar.url)),
                  title: Text(widget.media.channel.name, softWrap: false),
                  subtitle: Text("${widget.media.channel.subscriptionsCount} subscription(s)"),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) => ChannelPage(user: widget.media.channel.name),
                    ));
                  },
                )),
                if (watch_in.State().isNotAuthor(widget.media.channel.name))
                  Flexible(flex: 1, child: IconButton(
                    icon: Icon(_subscription ? Icons.person_remove_alt_1 : Icons.person_add_alt_1),
                    onPressed: () {
                      ChannelService.setSubscription(widget.media.channel.name, !_subscription);
                      setState(() {
                        _subscription = !_subscription;
                      });
                    },
                  )),
                if (watch_in.State().isNotAuthor(widget.media.channel.name))
                  Flexible(flex: 1, child: IconButton(
                    icon: Icon(_favorite ? Icons.favorite : Icons.favorite_border),
                    onPressed: () {
                      VideoService.setFavorite(widget.media, !widget.media.favorite);
                      setState(() {
                        _favorite = !_favorite;
                      });
                    },
                  )),
                Flexible(flex: 2, child: Column(children: [
                  Text('${widget.media.views} views'),
                  Text(formDate(widget.media.uploaded)),
                ])),
                Flexible(
                  flex: 2,
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                    TextButton.icon(
                      onPressed: widget.isLoggedIn
                          ? () { VideoService.like(widget.media, true, _liked); setState(() => _liked = !_liked ); }
                          : null,
                      icon: Icon(_liked ? Icons.thumb_up : Icons.thumb_up_outlined, size: 16.0),
                      label: Text("${widget.media.likes.likes}"),
                    ),
                    TextButton.icon(
                      onPressed: widget.isLoggedIn
                          ? () { VideoService.like(widget.media, false, _disliked); setState(() => _disliked = !_disliked ); }
                          : null,
                      icon: Icon(_disliked ? Icons.thumb_down : Icons.thumb_down_outlined, size: 16.0),
                      label: Text("${widget.media.likes.dislikes}"),
                    ),
                  ])
                ),
              ]),
              Center(child: Text(widget.media.description ?? "No description")),
              const SizedBox(height: 16),
              const Center(child: Text("Comments", style: TextStyle(fontWeight: FontWeight.bold))),
              PagedListView<Comment>(
                futureFunction: (paging) => VideoService.comments(paging, widget.media),
                headerBuilder: () =>
                  ListTile(
                    leading: avatar(),
                    title: TextField(
                      decoration: const InputDecoration(hintText: "Add comment"),
                      onSubmitted: (value) { VideoService.comment(widget.media, value); },
                      enabled: widget.isLoggedIn,
                    ),
                  ),
                itemBuilder: (comment) =>
                  ListTile(
                    leading: CircleAvatar(backgroundImage: NetworkImage(comment.channel.avatar.url)),
                    title: Text(comment.channel.name, softWrap: false),
                    subtitle: Text("${formDate(comment.creationDate)}\n${comment.content}"),
                  ),
                emptyText: "No comments on this media yet",
              ),
              const SizedBox(height: 16),
              const Center(child: Text("Related videos", style: TextStyle(fontWeight: FontWeight.bold))),
              PagedListView<Listable>(
                futureFunction: (paging) => VideosService.related(paging, widget.media),
                itemBuilder: (related) =>
                  videoList(
                    media: related,
                    onTap: () async {
                      final media = await related.toWatchable();
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => MediaPage(media: media, isLoggedIn: widget.isLoggedIn),
                      ));
                    },
                  ),
                emptyText: "No related videos"
              ),
            ]);
          }
        ),
      ),
    );
  }
}
