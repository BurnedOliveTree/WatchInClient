import 'package:flutter/material.dart';

import 'package:watch_in_client/model/api/channel/channel.dart';
import 'package:watch_in_client/model/api/channel/channel_management.dart';
import 'package:watch_in_client/model/api/media/video/videos.dart';
import 'package:watch_in_client/model/data/channel/channel_details.dart';
import 'package:watch_in_client/model/data/filter.dart';
import 'package:watch_in_client/model/data/media/base_media.dart';
import 'package:watch_in_client/model/state.dart' as watch_in;
import 'package:watch_in_client/model/utilities/datetime.dart';
import 'package:watch_in_client/view/components/management_bar.dart';
import 'package:watch_in_client/view/components/paged_list_view.dart';
import 'package:watch_in_client/view/components/standard_future_builder.dart';
import 'package:watch_in_client/view/components/video_list.dart';
import 'package:watch_in_client/view/pages/media.dart';


class ChannelPage extends StatefulWidget {
  const ChannelPage({required this.user, Key? key}) : super(key: key);

  final String user;

  @override
  State<ChannelPage> createState() => _ChannelPageState();
}

class _ChannelPageState extends State<ChannelPage> {
  bool _edit = false;

  Widget _showIfAuthor(Widget child) {
    if (watch_in.State().isAuthor(widget.user)) {
      return child;
    } else {
      return Container();
    }
  }

  AppBar _showBar(BuildContext context) {
    if (watch_in.State().isAuthor(widget.user)) {
      return managementBar(context);
    } else {
      return AppBar(title: Text("${widget.user}'s channel"));
    }
  }

  Widget _text(String text) {
    if (!_edit) {
      return Text(text);
    } else {
      return TextField(
        decoration: InputDecoration(hintText: text),
        onSubmitted: (value) { ChannelManagementService.setDescription(value); },
        keyboardType: TextInputType.text,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _showBar(context),
      body: defaultFutureBuilder<ChannelDetails>(
        futureFunction: () { return ChannelService.details(widget.user); },
        child: (channel) {
          return ListView(children: [
            if (channel.background != null)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: double.maxFinite,
                  height: 150,
                  child: Image(
                    image: NetworkImage(channel.background!.url),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ListTile(
              leading: CircleAvatar(backgroundImage: NetworkImage(channel.avatar.url)),
              title: Text(channel.name, softWrap: false),
              subtitle: _text(channel.description ?? "No description"),
              isThreeLine: channel.description != null,
            ),
            GridView(
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                mainAxisExtent: 72,
                maxCrossAxisExtent: 360,
              ),
              padding: const EdgeInsets.all(8),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                Card(child: ListTile(
                  leading: const Icon(Icons.calendar_today),
                  title: const Text("Joined", softWrap: false),
                  subtitle: Text(formDate(channel.creationDate), softWrap: false),
                )),
                Card(child: ListTile(
                  leading: const Icon(Icons.video_collection),
                  title: const Text("Videos", softWrap: false),
                  subtitle: Text("${channel.videosCount}", softWrap: false),
                )),
                Card(child: ListTile(
                  leading: const Icon(Icons.remove_red_eye),
                  title: const Text("Views", softWrap: false),
                  subtitle: Text("${channel.viewsCount}", softWrap: false),
                )),
                Card(child: ListTile(
                  leading: const Icon(Icons.stars),
                  title: const Text("Subscriptions", softWrap: false),
                  subtitle: Text("${channel.subscriptionsCount}", softWrap: false),
                )),
              ],
            ),
            PagedListView<Listable>(
              futureFunction: (paging) => VideosService.search(paging, SearchFilter(channel: channel.name, sort: SortOrder.relevance)),
              itemBuilder: (result) => videoList(
                media: result,
                onTap: () async {
                  final media = await result.toWatchable();
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) => MediaPage(media: media, isLoggedIn: true),
                  ));
                },
              ),
              emptyText: "No videos in this section"
            ),
          ]);
        }
      ),
      floatingActionButton: _showIfAuthor(FloatingActionButton(
        child: const Icon(Icons.edit),
        onPressed: () { setState(() { _edit = !_edit; }); },
      ))
    );
  }
}
